import sympy
import primesieve


def mathhomework(number):
    for y in primesieve.primes(number):
        b = int(number / y) if number / y % 1 == 0 else None
        if sympy.isprime(y) and sympy.isprime(b):
            return str(b), str(y)
    return ''


if __name__ == '__main__':

    num_of_test_cases = input("Enter number of test cases:    ")
    if num_of_test_cases.strip().isdigit():
        num_test_cases = input('Enter space separated test cases: (e.g 34 54 48)  ').split()

    value_list = [int(test_case) for test_case in num_test_cases]

    if len(value_list) != int(num_of_test_cases):
        print("You entered an incorrect number of testcases")
        exit()

    for x in value_list:
        q = mathhomework(x)
        result = " ".join(q)
        print(str(x)+" ---> ", result)





